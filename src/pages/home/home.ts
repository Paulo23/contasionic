import { EditPage } from './../edit/edit';
import { BillsServiceProvider } from './../../providers/bills-service/bills-service';
import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { App } from 'ionic-angular';
import { AddPage } from '../add/add';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  bills: any
  money = 0.0

  constructor(public navCtrl: NavController,
    public app: App,
    private loadingCtrl: LoadingController,
    private userService: UserServiceProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {

  }

  ionViewDidLoad(){
  }

  ionViewWillEnter() {
    this.loadBills()
  }

  popView(){
    let loader = this.loadingCtrl.create({
      content: "Deslogando...",
    });
    
    loader.present()
    this.storage.get("token").then(val => {
        this.userService.userLogout(val).subscribe((data) => {
          this.storage.remove('token');
          loader.dismiss();
          this.app.getRootNav().pop();
        })
    })
   }

   loadBills(){
     let self = this
      this.storage.get("token").then(val =>{
        this.billsService.getUserBills(val).map(res => res.json()).subscribe(data => {
          this.bills = data.result.data
          self.money = 0.0
          this.bills.forEach(function (value, i) {
            if(value['type'] == 'in'){
              self.money += parseFloat(value.value)
            } else {
              self.money -= parseFloat(value.value)
            }
          });
        })
      })
   }

   addAccount(){
      this.navCtrl.push(AddPage)
   }

   clickAccountList(bill: any)
   {
     this.navCtrl.push(EditPage, {bill: bill})
   }
}
