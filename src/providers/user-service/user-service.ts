import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the UserServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserServiceProvider {

  constructor(
    public http: Http) {

  }

  userLogin(userName: string, password: string){
    var url = "http://livia.walterneto.com.br/api/v1/login"
    let postParams = {
      username: userName,
      password: password
    }

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

   return this.http.post(url, JSON.stringify(postParams), {headers: headers})
  }

  userLogout(userToken){
    var url = "http://livia.walterneto.com.br/api/v1/logout?api_token="+userToken

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.post(url, null,{headers: headers})
  }
}
