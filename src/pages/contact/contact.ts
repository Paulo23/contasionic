import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { Storage } from '@ionic/storage';
import { BillsServiceProvider } from './../../providers/bills-service/bills-service';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  colorRgb = ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)','rgba(255, 206, 86, 0.2)', "rgb(0,0,205)", "rgb(255,0,0)", "rgb(0,128,0)"]
  hoverColor = ["#FF6384","#36A2EB","#FFCE56", "#0000CD", "	#FF0000", "#008000"]
  constructor(public navCtrl: NavController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.storage.get("token").then(val =>{
        this.billsService.getUserBillsStatistic(val).map(res => res.json()).subscribe(data => {
          let result = data.result.data;
          let labels = []
          let percents = []
          result.forEach(function (value, i) {
              labels.push(value.category)
              percents.push(value.percent)
          });

          this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    label: '# of Votes',
                    data: percents,
                    backgroundColor: this.colorRgb,
                    hoverBackgroundColor: this.hoverColor
                }]
            }

         });
        })
      })
  }
}
