export class Accounts {
    name: string
    date: string
    type: string
    value: number

    constructor(name:string, date:string, type:string, value:number)
    {
        this.name = name
        this.date = date
        this.type = type
        this.value = value
    }
}