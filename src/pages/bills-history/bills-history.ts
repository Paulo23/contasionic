import { Component } from '@angular/core';
import { BillsServiceProvider } from './../../providers/bills-service/bills-service';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { App } from 'ionic-angular';
import { AddPage } from '../add/add';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LoadingController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the BillsHistoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-bills-history',
  templateUrl: 'bills-history.html',
})
export class BillsHistoryPage {
  tabBarElement: any;
  bills: any
  month: any
  money = 0.0

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public app: App,
    private loadingCtrl: LoadingController,
    private userService: UserServiceProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {
      this.month = navParams.get('month')
      this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewDidLoad(){
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
    this.loadBills()
  }
  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }

  popView(){
    let loader = this.loadingCtrl.create({
      content: "Deslogando...",
    });
    
    loader.present()
    this.storage.get("token").then(val => {
        this.userService.userLogout(val).subscribe((data) => {
          this.storage.remove('token');
          loader.dismiss();
          this.app.getRootNav().pop();
        })
    })
   }

   loadBills(){
     let self = this
      this.storage.get("token").then(val =>{
        this.billsService.getUserBillsByMonth(self.month, val).map(res => res.json()).subscribe(data => {
          this.bills = data.result.data
          self.money = 0.0
          this.bills.forEach(function (value, i) {
            if(value['type'] == 'in'){
              self.money += parseFloat(value.value)
            } else {
              self.money -= parseFloat(value.value)
            }
          });
        })
      })
   }

   addAccount(){
      this.navCtrl.push(AddPage)
   }

   clickAccountList()
   {
     this.navCtrl.push(AddPage)
   }
}
