import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BillsServiceProvider } from "../../providers/bills-service/bills-service";

/**
 * Generated class for the AddPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-add',
  templateUrl: 'add.html',
})
export class AddPage {
  tabBarElement: any;

  description: any
  type: any
  valueBill: any
  category: any
  date: any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPage');
  }

  saveAccount()
  {
    this.storage.get("token").then(val =>{
        this.billsService.saveBill(val, this.description, this.type, this.valueBill, this.category, this.date).map(res => res.json()).subscribe(data => {
          let alert = this.alertCtrl.create({
          title: 'Mensagem',
          subTitle: 'Conta salva com sucesso!',
          buttons: [{
            text: 'Ok',
            handler: () => {
              this.navCtrl.pop()
            }
          }]
        })
        alert.present();
        });
      })
  }

  typeSelected()
  {
    
  }
}
