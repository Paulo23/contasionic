import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillsHistoryPage } from './bills-history';

@NgModule({
  declarations: [
    BillsHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(BillsHistoryPage),
  ],
  exports: [
    BillsHistoryPage
  ]
})
export class BillsHistoryPageModule {}
