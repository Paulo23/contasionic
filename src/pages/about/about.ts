import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { BillsServiceProvider } from './../../providers/bills-service/bills-service';
import { BillsHistoryPage } from "../bills-history/bills-history";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  items: any;

  constructor(public navCtrl: NavController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {

  }

  ionViewDidLoad(){
    
  }

  ionViewWillEnter() {
    this.loadMonths()
  }

  itemSelected(item: any) {
    this.navCtrl.push(BillsHistoryPage, {month: parseInt(item.month)})
  }

  loadMonths(){
      this.storage.get("token").then(val =>{
        this.billsService.getUserBillsHistory(val).map(res => res.json()).subscribe(data => {
          this.items = data
        })
      })
   }
}
