import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

/*
  Generated class for the BillsServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class BillsServiceProvider {

  constructor(public http: Http) {
    
  }

  getUserBills(token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, {headers: headers})
  }

  getBill(id: string, token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills/"+id+"?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, {headers: headers})
  }

  getUserBillsByMonth(month: number, token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills?month="+month+"&api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, {headers: headers})
  }

  getUserBillsHistory(token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills/history?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, {headers: headers})
  }

  getUserBillsStatistic(token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills/statistics?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, {headers: headers})
  }

  saveBill(token: string, description: string, type: string, value: string, bill_category_id: string, issue_date: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let postParams = {
      description: description,
      type: type,
      value: value,
      issue_date: issue_date,
      bill_category_id: bill_category_id
    }

    return this.http.post(url, postParams, {headers: headers})
  }

  editBill(bill: any, token: string){
    var url = "http://livia.walterneto.com.br/api/v1/bills/"+bill.id+"?api_token="+token

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let postParams = {
      description: bill.description,
      type: bill.type,
      value: bill.value,
      bill_category_id: bill.bill_category_id
    }

    return this.http.post(url, postParams, {headers: headers})
  }
}
