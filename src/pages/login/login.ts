import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: string
  password: string
  tabsPage: any
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public userService: UserServiceProvider, 
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private storage: Storage,) {
    
  }

  destroy(){
    this.navCtrl.push(TabsPage)
  }

  ionViewDidLoad() {
    let self = this
    this.storage.get("token").then(val => {
      if(val) {
        self.destroy();
      }
    })
  }


  clickLogin(){
    let loader = this.loadingCtrl.create({
      content: "Entrando...",
    });
    loader.present();
    
    this.userService.userLogin(this.user, this.password).subscribe((data) => {
        loader.dismiss()
        let result: string = data.json()['token']
        let alert = this.alertCtrl.create({
          title: 'Mensagem',
          subTitle: 'Login realizado com sucesso!',
          buttons: [{
            text: 'Ok',
            handler: () => {
              this.storage.set('token', result);
              this.navCtrl.push(TabsPage)
            }
          }]
        })
        alert.present();
       }, error => {
        console.log(error);// Error getting the data
    });
  }
}
