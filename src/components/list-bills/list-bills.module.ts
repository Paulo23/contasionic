import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListBillsComponent } from './list-bills';

@NgModule({
  declarations: [
    ListBillsComponent,
  ],
  imports: [
    IonicPageModule.forChild(ListBillsComponent),
  ],
  exports: [
    ListBillsComponent
  ]
})
export class ListBillsComponentModule {}
