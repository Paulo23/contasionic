import { Component, Input } from '@angular/core';
import { AlertController } from "ionic-angular";

/**
 * Generated class for the ListBillsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'list-bills',
  templateUrl: 'list-bills.html'
})
export class ListBillsComponent {

  text: string;
  @Input() bill: any
  constructor( private alertCtrl: AlertController) {
    this.text = 'Hello World';
  }

  ionViewDidLoad(){
    this.text = this.bill
  }
}
