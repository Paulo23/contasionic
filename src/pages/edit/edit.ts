import { BillsServiceProvider } from './../../providers/bills-service/bills-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the EditPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-edit',
  templateUrl: 'edit.html',
})
export class EditPage {
  tabBarElement: any;
  bill: any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController,
    private storage: Storage,
    private billsService: BillsServiceProvider) {
      this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
      this.bill = navParams.get('bill')
  }

  ionViewDidLoad() { 
  }

  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }

  ionViewWillLeave() {
    this.tabBarElement.style.display = 'flex';
  }

  editAccount()
  {
    let self = this
    this.storage.get("token").then(val =>{
        this.billsService.editBill(self.bill, val).map(res => res.json()).subscribe(data => {
          let alert = this.alertCtrl.create({
          title: 'Mensagem',
          subTitle: 'Conta editada com sucesso!',
          buttons: [{
            text: 'Ok',
            handler: () => {
              this.navCtrl.pop()
            }
          }]
        })
        alert.present();
        });
      })
  }

}
